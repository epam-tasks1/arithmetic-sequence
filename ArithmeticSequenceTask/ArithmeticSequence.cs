﻿using System;

namespace ArithmeticSequenceTask
{
    public static class ArithmeticSequence
    {
        public static int Calculate(int number, int add, int count)
        {
            if ((number is int.MaxValue && add > 0) || (number is int.MinValue && add < 0))
            {
                throw new OverflowException("The obtained result out of range of integer values.");
            }

            if (count < 0)
            {
                throw new ArgumentException("count is less then 0.", nameof(count));
            }

            int calculateResult = (count * number) + ((count * (count - 1) / 2) * add);

            return calculateResult;
        }
    }
}
